using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyApp
{
    public static class Errors
    {
        private static readonly List<string> ErrorsData = new List<string>();
        public static void ReportError(IToken token, string message)
        {
            var error = $"{token.Line}:{token.Column} - {message}";
            ErrorsData.Add(error);
            // Console.ForegroundColor = ConsoleColor.Red;
            // Console.WriteLine(error);
            // Console.ForegroundColor = ConsoleColor.Gray;
        }
        public static int NumberOfErrors {  get { return ErrorsData.Count; } }
        public static void PrintAndClearErrors()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            foreach (var error in ErrorsData)
            {
                Console.WriteLine(error);
            }
            Console.ForegroundColor = ConsoleColor.Gray;
            ErrorsData.Clear(); 
        }
    }
}