using System;
using System.Text;
using Antlr4.Runtime.Misc;

namespace MyApp
{
    public class EvalVisitor : MyGrammarBaseVisitor<(Types Type,object Value)>
    { 
        SymbolTable symbolTable = new SymbolTable();

        private float ToFloat(object value)
        {
            if (value is int x) return x;
            return (float)value;
        }
        private string ToString(object value)
        {
            return (string)value;
        }
        private bool ToBool(object value)
        {
            return (bool)value;
        }
        
        public override (Types Type, object Value) VisitProgram([NotNull] MyGrammarParser.ProgramContext context)
        {
            foreach (var statement in context.statement())
            {
                Visit(statement);
            }
            return (Types.Error, 0); // Return value as needed
        }
        public override (Types Type, object Value) VisitDeclaration([NotNull] MyGrammarParser.DeclarationContext context)
        {
            var type = Visit(context.type());
            foreach(var identifier in context.IDENTIFIER())
            {
                symbolTable.Add(identifier.Symbol, type.Type);
            }
            return (Types.Error, 0);
        }
        
        public override (Types Type, object Value) VisitType([NotNull] MyGrammarParser.TypeContext context)
        {
            if (context.INT() != null) return (Types.Int,0);
            if (context.FLOAT() != null) return (Types.Float,(float)0);
            if (context.BOOL() != null) return (Types.Bool,false);
            if (context.STRING() != null) return (Types.String, "");
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitLiteral([NotNull] MyGrammarParser.LiteralContext context)
        {
            //Console.WriteLine(context.GetText());
            
            if(context.DECIMAL_LITERAL() != null) return (Types.Int, int.Parse(context.DECIMAL_LITERAL().GetText()));
            if(context.STRING_LITERAL() != null) return (Types.String, context.STRING_LITERAL().GetText());
            if(context.BOOL_LITERAL() != null)  return (Types.Bool, bool.Parse(context.BOOL_LITERAL().GetText()));
            if (context.FLOAT_LITERAL() != null) return (Types.Float, float.Parse(context.FLOAT_LITERAL().GetText()));
            
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitLiteralexpr([NotNull] MyGrammarParser.LiteralexprContext context)
        {
            return VisitLiteral(context.literal());
        }
        public override (Types Type, object Value) VisitIdexpr([NotNull] MyGrammarParser.IdexprContext context)
        {
            return symbolTable[context.IDENTIFIER().Symbol];
        }
        public override (Types Type, object Value) VisitParexpr([NotNull] MyGrammarParser.ParexprContext context)
        {
            return Visit(context.expression());
        }
        public override (Types Type, object Value) VisitAddminexpr([NotNull] MyGrammarParser.AddminexprContext context)
        {
            var left = Visit(context.expression()[0]);
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Float || right.Type == Types.Float)
            {
                if ((left.Type == Types.Float || left.Type == Types.Int) && (right.Type == Types.Float || right.Type == Types.Int))
                {
                    if (context.op.Type == MyGrammarParser.PLUS) return (Types.Float, 0);
                    if (context.op.Type == MyGrammarParser.MINUS) return (Types.Float, 0);
                    Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                    return (Types.Error, 0);
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            if(left.Type == Types.Int || right.Type == Types.Int)
            {
                if (left.Type == Types.Int && right.Type == Types.Int)
                {
                    if (context.op.Type == MyGrammarParser.PLUS) return (Types.Int, 0);
                    if (context.op.Type == MyGrammarParser.MINUS) return (Types.Int, 0);
                    Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                    return (Types.Error, 0);
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            if(left.Type == Types.String || right.Type == Types.String)
            {
                if ((left.Type == Types.String && right.Type == Types.String) && context.op.Type != MyGrammarParser.PLUS && context.op.Type != MyGrammarParser.MINUS)
                {
                    return (Types.String, "");
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitMuldivexpr([NotNull] MyGrammarParser.MuldivexprContext context)
        {
            var left = Visit(context.expression()[0]);
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Float || right.Type == Types.Float)
            {
                if ((left.Type == Types.Float || left.Type == Types.Int) && (right.Type == Types.Float || right.Type == Types.Int))
                {
                    if (context.op.Type == MyGrammarParser.MULT) return (Types.Float, 0);
                    if (context.op.Type == MyGrammarParser.DIV) return (Types.Float, 0);
                    Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                    return (Types.Error, 0);
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            if(left.Type == Types.Int || right.Type == Types.Int)
            {
                if (left.Type == Types.Int && right.Type == Types.Int)
                {
                    if (context.op.Type == MyGrammarParser.MULT) return (Types.Int, 0);
                    if (context.op.Type == MyGrammarParser.DIV) return (Types.Int, 0);
                    if (context.op.Type == MyGrammarParser.MOD) return (Types.Int, 0);
                    Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                    return (Types.Error, 0);
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitLtgtexpr([NotNull] MyGrammarParser.LtgtexprContext context)
        {
            var left = Visit(context.expression()[0]);
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Float || right.Type == Types.Float)
            {
                if ((left.Type == Types.Float || left.Type == Types.Int) && (right.Type == Types.Float || right.Type == Types.Int))
                {
                    if (context.op.Type == MyGrammarParser.LT) return (Types.Bool, false);
                    if (context.op.Type == MyGrammarParser.GT) return (Types.Bool, false);
                    Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                    return (Types.Error, 0);
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            if(left.Type == Types.Int || right.Type == Types.Int)
            {
                if (left.Type == Types.Int && right.Type == Types.Int)
                {
                    if (context.op.Type == MyGrammarParser.LT) return (Types.Bool, false);
                    if (context.op.Type == MyGrammarParser.GT) return (Types.Bool, false);
                    Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                    return (Types.Error, 0);
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitEqneqexpr([NotNull] MyGrammarParser.EqneqexprContext context)
        {
            var left = Visit(context.expression()[0]);
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Float || right.Type == Types.Float)
            {
                if ((left.Type == Types.Float || left.Type == Types.Int) && (right.Type == Types.Float || right.Type == Types.Int))
                {
                    if (context.op.Type == MyGrammarParser.EQ) return (Types.Bool, false);
                    if (context.op.Type == MyGrammarParser.NEQ) return (Types.Bool, false);
                    Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                    return (Types.Error, 0);
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            if(left.Type == Types.Int || right.Type == Types.Int)
            {
                if (left.Type == Types.Int && right.Type == Types.Int)
                {
                    if (context.op.Type == MyGrammarParser.EQ) return (Types.Bool, false);
                    if (context.op.Type == MyGrammarParser.NEQ) return (Types.Bool, false);
                    Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                    return (Types.Error, 0);
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            if(left.Type == Types.String || right.Type == Types.String)
            {
                if (left.Type == Types.String && right.Type == Types.String)
                {
                    if (context.op.Type == MyGrammarParser.EQ) return (Types.Bool, false);
                    if (context.op.Type == MyGrammarParser.NEQ) return (Types.Bool, false);
                    Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                    return (Types.Error, 0);
                }
                Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
                return (Types.Error, 0);
            }
            Errors.ReportError(context.op, $"Operator '{context.op.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitAndorexpr([NotNull] MyGrammarParser.AndorexprContext context)
        {
            var left = Visit(context.expression()[0]);
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Bool && right.Type == Types.Bool)
            {
                if (context.LOGIC_OR() != null) return (Types.Bool, false);
                if (context.LOGIC_AND() != null) return (Types.Bool, false);
                return (Types.Error, 0);
            }
            if (context.LOGIC_OR() != null)
                Errors.ReportError(context.LOGIC_OR().Symbol, $"Operator '{context.LOGIC_OR().Symbol.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
            if (context.LOGIC_AND() != null)
                Errors.ReportError(context.LOGIC_AND().Symbol, $"Operator '{context.LOGIC_AND().Symbol.Text}' cannot be applied to types {left.Type.ToString()} and {right.Type.ToString()}.");
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitNotexpr([NotNull] MyGrammarParser.NotexprContext context)
        {
            var expr = Visit(context.expression());
            if (expr.Type == Types.Error) return (Types.Error, 0);
            if (expr.Type == Types.Bool)
            {
                return (Types.Bool, false);
            }
            Errors.ReportError(context.NOT().Symbol, $"Operator '{context.NOT().GetText()}' cannot be applied to type {expr.Type.ToString()}.");
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitUminusexpr([NotNull] MyGrammarParser.UminusexprContext context)
        {
            var expr = Visit(context.expression());
            if (expr.Type == Types.Error) return (Types.Error, 0);
            if (expr.Type == Types.Float)
            {
                return (Types.Float, 0);
            }
            if (expr.Type == Types.Int)
            {
                return (Types.Int, 0);
            }
            
            Errors.ReportError(context.MINUS().Symbol, $"Operator '{context.MINUS().GetText()}' cannot be applied to type {expr.Type.ToString()}.");
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitAssignment([NotNull] MyGrammarParser.AssignmentContext context)
        {
            var right = Visit(context.expression());
            var ret = (Types.Error, 0);
            var id = context.IDENTIFIER();
            var variable = symbolTable[id.Symbol];
            if (variable.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (variable.Type == Types.Int && right.Type != Types.Int)
            {
                //Console.WriteLine("aaaaaaa");
                Errors.ReportError(id.Symbol, $"Variable '{id.GetText()}' type is int, but the assigned value is {right.Type.ToString()}.");
            }
            if (variable.Type == Types.Float)
            {
                if (right.Type == Types.Int || right.Type == Types.Float)
                {
                    var value = (Types.Float, ToFloat(right.Value));
                    symbolTable[id.Symbol] = value;
                    return value;
                } 
                Errors.ReportError(id.Symbol, $"Variable '{id.GetText()}' type is float, but the assigned value is {right.Type.ToString()}.");
                
            }
            if (variable.Type == Types.String && right.Type != Types.String)
            {
                Errors.ReportError(id.Symbol, $"Variable '{id.GetText()}' type is string, but the assigned value is {right.Type.ToString()}.");
            }
            if (variable.Type == Types.Bool && right.Type != Types.Bool)
            {
                Errors.ReportError(id.Symbol, $"Variable '{id.GetText()}' type is bool, but the assigned value is {right.Type.ToString()}.");
            }
            symbolTable[id.Symbol] = right;
            ret = (right.Type, 0);
            
            return ret;
        }
        public override (Types Type, object Value) VisitAssignexpr([NotNull] MyGrammarParser.AssignexprContext context)
        {
            return VisitAssignment(context.assignment());
        }
        public override (Types Type, object Value) VisitDeclaration_stat([NotNull] MyGrammarParser.Declaration_statContext context)
        {
            return VisitDeclaration(context.declaration());
        }
        public override (Types Type, object Value) VisitExpression_stat([NotNull] MyGrammarParser.Expression_statContext context)
        {
            return Visit(context.expression());
        }
        public override (Types Type, object Value) VisitWrite_stat(
            [NotNull] MyGrammarParser.Write_statContext context)
        {
            return VisitWrite_statement(context.write_statement());
        }

        public override (Types Type, object Value) VisitWrite_statement([NotNull] MyGrammarParser.Write_statementContext context)
        {
            foreach (var expr in context.expression())
            {
                var value = Visit(expr);
                if (value.Type == Types.Error)
                {
                    Errors.ReportError(expr.Start, $"Could not write expression {expr.GetText()}.");
                }
            }
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitRead_stat([NotNull] MyGrammarParser.Read_statContext context)
        {
            return VisitRead_statement(context.read_statement());
        }
        public override (Types Type, object Value) VisitRead_statement([NotNull] MyGrammarParser.Read_statementContext context)
        {
            foreach (var id in context.IDENTIFIER())
            {
                var found = symbolTable[id.Symbol];
                if(found.Type == Types.Error)
                {
                    Errors.ReportError(context.Start, $"Could not load variable '{id}'. No such variable exists.");
                }
            }
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitIf_stat([NotNull] MyGrammarParser.If_statContext context)
        {
            return VisitIf_statement(context.if_statement());
        }
        public override (Types Type, object Value) VisitIf_statement([NotNull] MyGrammarParser.If_statementContext context)
        {
            var condition = Visit(context.expression());
            if (condition.Type != Types.Bool)
            {
                Errors.ReportError(context.expression().Start, $"Wrong condition");
                return (Types.Error, 0);
            }
            return (Types.Error, 0);
        }

        public override (Types Type, object Value) VisitBlock_stat(MyGrammarParser.Block_statContext context)
        {
            return VisitBlock(context.block());
        }
        
        public override (Types Type, object Value) VisitBlock(MyGrammarParser.BlockContext context)
        {
            foreach (var statement in context.statement())
            {
                Visit(statement);
            }
            return (Types.Error, 0);
        }

        public override (Types Type, object Value) VisitWhile_stat([NotNull] MyGrammarParser.While_statContext context)
        {
            return VisitWhile_statement(context.while_statement());
        }

        public override (Types Type, object Value) VisitWhile_statement(MyGrammarParser.While_statementContext context)
        {
            var val = Visit(context.expression()); // condition
            if (val.Type != Types.Bool)
            {
                Errors.ReportError(context.expression().Start, $"Wrong condition");
                return (Types.Error, 0);
            }
            Visit(context.statement());
            return (Types.Error, 0);  
        }

        public override (Types Type, object Value) VisitDo_while_stat(
            [NotNull] MyGrammarParser.Do_while_statContext context)
        {
            return VisitDo_while_statement(context.do_while_statement());
        }
        public override (Types Type, object Value) VisitDo_while_statement(
            [NotNull] MyGrammarParser.Do_while_statementContext context)
        {
            var val = Visit(context.expression());
            var ret = (Types.Error, 0);
            if (val.Type != Types.Bool)
            {
                Errors.ReportError(context.expression().Start, $"Wrong condition");
                return (Types.Error, 0);
            }
            return ret;

        }
        
    }
}
