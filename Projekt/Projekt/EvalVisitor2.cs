using System;
using System.Text;
using Antlr4.Runtime.Misc;

namespace MyApp
{
    public class EvalVisitor2 : MyGrammarBaseVisitor<(Types Type,object Value)>
    { 
        SymbolTable symbolTable = new SymbolTable();
        private List<string> instructions = new();
        private int labelCounter = -1;
        
        private float ToFloat(object value)
        {
            if (value is int x) return x;
            return (float)value;
        }
        private string ToString(object value)
        {
            return (string)value;
        }
        private bool ToBool(object value)
        {
            return (bool)value;
        }
        
        public override (Types Type, object Value) VisitProgram([NotNull] MyGrammarParser.ProgramContext context)
        {
            foreach (var statement in context.statement())
            {
                Visit(statement);
            }
            return (Types.Error, 0); // Return value as needed
        }
        public override (Types Type, object Value) VisitDeclaration([NotNull] MyGrammarParser.DeclarationContext context)
        {
            var type = Visit(context.type());
            foreach(var identifier in context.IDENTIFIER())
            {
                symbolTable.Add(identifier.Symbol, type.Type);
                if (type.Type == Types.Int)
                {
                    instructions.Add($"push I 0");
                    instructions.Add($"save {identifier.Symbol.Text}");
                }
                if (type.Type == Types.Float)
                {
                    instructions.Add($"push F 0.0");
                    instructions.Add($"save {identifier.Symbol.Text}");
                }
                if (type.Type == Types.Bool)
                {
                    instructions.Add($"push B false");
                    instructions.Add($"save {identifier.Symbol.Text}");
                }
                if (type.Type == Types.String)
                {
                    instructions.Add($"push S \"\"");
                    instructions.Add($"save {identifier.Symbol.Text}");
                }
            }
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitType([NotNull] MyGrammarParser.TypeContext context)
        {
            if (context.INT() != null)
            {
                return (Types.Int, 0);
            }
            if (context.FLOAT() != null)
            {
                return (Types.Float, 0);
            }
            if (context.BOOL() != null)
            {
                return (Types.Bool, false);
            }
            if (context.STRING() != null)
            {
                return (Types.String, "");
            }
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitLiteral([NotNull] MyGrammarParser.LiteralContext context)
        {
            //Console.WriteLine(context.GetText());

            if (context.DECIMAL_LITERAL() != null)
            {
                instructions.Add($"push I {context.DECIMAL_LITERAL().GetText()}");
                return (Types.Int, 0);
            }
            if (context.STRING_LITERAL() != null)
            {
                instructions.Add($"push S {context.STRING_LITERAL().GetText()}");
                return (Types.String, "");
            }
            if (context.BOOL_LITERAL() != null)
            {
                instructions.Add($"push B {context.BOOL_LITERAL().GetText()}");
                return (Types.Bool, false);
            }
            if (context.FLOAT_LITERAL() != null)
            {
                instructions.Add($"push F {context.FLOAT_LITERAL().GetText()}");
                return (Types.Float, 0);
            }
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitLiteralexpr([NotNull] MyGrammarParser.LiteralexprContext context)
        {
            return VisitLiteral(context.literal());
        }
        public override (Types Type, object Value) VisitIdexpr([NotNull] MyGrammarParser.IdexprContext context)
        {
            instructions.Add($"load {context.IDENTIFIER().Symbol.Text}");
            return symbolTable[context.IDENTIFIER().Symbol];
        }
        public override (Types Type, object Value) VisitParexpr([NotNull] MyGrammarParser.ParexprContext context)
        {
            return Visit(context.expression());
        }
        public override (Types Type, object Value) VisitAddminexpr([NotNull] MyGrammarParser.AddminexprContext context)
        {
            var left = Visit(context.expression()[0]);
            int index = instructions.Count;
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Float || right.Type == Types.Float)
            {
                if (left.Type == Types.Int )
                {
                    instructions.Insert(index, "itof");
                }
                if (right.Type == Types.Int)
                {
                    instructions.Add("itof");
                }
                if (context.op.Type == MyGrammarParser.PLUS)
                {
                    instructions.Add($"add");
                    return (Types.Float, 0);
                }
                if (context.op.Type == MyGrammarParser.MINUS)
                {
                    instructions.Add($"sub");
                    return (Types.Float, 0);
                }
                return (Types.Error, 0);
            }
            if(left.Type == Types.Int || right.Type == Types.Int)
            {
                if (left.Type == Types.Int && right.Type == Types.Int)
                {
                    if (context.op.Type == MyGrammarParser.PLUS)
                    {
                        instructions.Add($"add");
                        return (Types.Int, 0);
                    }
                    if (context.op.Type == MyGrammarParser.MINUS)
                    {
                        instructions.Add($"sub");
                        return (Types.Int, 0);
                    }
                    return (Types.Error, 0);
                }
                return (Types.Error, 0);
            }
            if(left.Type == Types.String || right.Type == Types.String)
            {
                if ((left.Type == Types.String && right.Type == Types.String) && context.op.Type != MyGrammarParser.PLUS && context.op.Type != MyGrammarParser.MINUS)
                {
                    instructions.Add("concat");
                    return (Types.String, "");
                }
                return (Types.Error, 0);
            }
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitMuldivexpr([NotNull] MyGrammarParser.MuldivexprContext context)
        {
            var left = Visit(context.expression()[0]);
            int index = instructions.Count;
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Float || right.Type == Types.Float)
            {
                if (left.Type == Types.Int )
                {
                    instructions.Insert(index, "itof");
                }
                if (right.Type == Types.Int)
                {
                    instructions.Add("itof");
                }
                if (context.op.Type == MyGrammarParser.MULT)
                {
                    instructions.Add($"mul");
                    return (Types.Float, 0);
                }
                if (context.op.Type == MyGrammarParser.DIV)
                {
                    instructions.Add($"div");
                    return (Types.Float, 0);
                }
                return (Types.Error, 0);
            }
            if(left.Type == Types.Int || right.Type == Types.Int)
            {
                if (left.Type == Types.Int && right.Type == Types.Int)
                {
                    if (context.op.Type == MyGrammarParser.MULT)
                    {
                        instructions.Add($"mul");
                        return (Types.Int, 0);
                    }
                    if (context.op.Type == MyGrammarParser.DIV)
                    {
                        instructions.Add($"div");
                        return (Types.Int, 0);
                    }

                    if (context.op.Type == MyGrammarParser.MOD)
                    {
                        instructions.Add($"mod");
                        return (Types.Int, 0);
                    }
                    return (Types.Error, 0);
                }
                return (Types.Error, 0);
            }
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitLtgtexpr([NotNull] MyGrammarParser.LtgtexprContext context)
        {
            var left = Visit(context.expression()[0]);
            int index = instructions.Count;
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Float || right.Type == Types.Float)
            {
                if (left.Type == Types.Int )
                {
                    instructions.Insert(index, "itof");
                }
                if (right.Type == Types.Int)
                {
                    instructions.Add("itof");
                }
                if (context.op.Type == MyGrammarParser.LT)
                {
                    instructions.Add($"lt");
                    return (Types.Bool, false);
                }
                if (context.op.Type == MyGrammarParser.GT)
                {
                    instructions.Add($"gt");
                    return (Types.Bool, false);
                }
                return (Types.Error, 0);
            }
            if(left.Type == Types.Int || right.Type == Types.Int)
            {
                if (left.Type == Types.Int && right.Type == Types.Int)
                {
                    if (context.op.Type == MyGrammarParser.LT)
                    {
                        instructions.Add($"lt");
                        return (Types.Bool, false);
                    }

                    if (context.op.Type == MyGrammarParser.GT)
                    {
                        instructions.Add($"gt");
                        return (Types.Bool, false);
                    }
                    return (Types.Error, 0);
                }
                return (Types.Error, 0);
            }
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitEqneqexpr([NotNull] MyGrammarParser.EqneqexprContext context)
        {
            var left = Visit(context.expression()[0]);
            int index = instructions.Count;
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Float || right.Type == Types.Float)
            {
                if (left.Type == Types.Int )
                {
                    instructions.Insert(index, "itof");
                }
                if (right.Type == Types.Int)
                {
                    instructions.Add("itof");
                }
                if (context.op.Type == MyGrammarParser.EQ)
                {
                    instructions.Add("eq");
                    return (Types.Bool, false);
                }
                if (context.op.Type == MyGrammarParser.NEQ)
                {
                    instructions.Add("eq");
                    instructions.Add("not");
                    return (Types.Bool, false);
                }
                return (Types.Error, 0);
            }
            if(left.Type == Types.Int || right.Type == Types.Int)
            {
                if (left.Type == Types.Int && right.Type == Types.Int)
                {
                    if (context.op.Type == MyGrammarParser.EQ)
                    {
                        instructions.Add("eq");
                        return (Types.Bool, false);
                    }
                    if (context.op.Type == MyGrammarParser.NEQ)
                    {
                        instructions.Add("eq");
                        instructions.Add("not");
                        return (Types.Bool, false);
                    }
                    return (Types.Error, 0);
                }
                return (Types.Error, 0);
            }
            if(left.Type == Types.String || right.Type == Types.String)
            {
                if (left.Type == Types.String && right.Type == Types.String)
                {
                    if (context.op.Type == MyGrammarParser.EQ)
                    {
                        instructions.Add("eq");
                        return (Types.Bool, false);
                    }
                    if (context.op.Type == MyGrammarParser.NEQ)
                    {
                        instructions.Add("eq");
                        instructions.Add("not");
                        return (Types.Bool, false);
                    }
                    return (Types.Error, 0);
                }
                return (Types.Error, 0);
            }
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitAndorexpr([NotNull] MyGrammarParser.AndorexprContext context)
        {
            var left = Visit(context.expression()[0]);
            var right = Visit(context.expression()[1]);
            if (left.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (left.Type == Types.Bool && right.Type == Types.Bool)
            {
                if (context.LOGIC_AND() != null)
                {
                    instructions.Add("and");
                    return (Types.Bool, false);
                } 
                if (context.LOGIC_OR() != null)
                {
                    instructions.Add("or");
                    return (Types.Bool, false);
                }
                return (Types.Error, 0);
            }
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitNotexpr([NotNull] MyGrammarParser.NotexprContext context)
        {
            var expr = Visit(context.expression());
            if (expr.Type == Types.Error) return (Types.Error, 0);
            if (expr.Type == Types.Bool)
            {
                instructions.Add("not");
                return (Types.Bool, false);//return (Types.Bool, !ToBool(expr.Value));
            }
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitUminusexpr([NotNull] MyGrammarParser.UminusexprContext context)
        {
            var expr = Visit(context.expression());
            if (expr.Type == Types.Error) return (Types.Error, 0);
            if (expr.Type == Types.Float)
            {
                instructions.Add("uminus");
                return (Types.Float, 0);//return (Types.Float, ToFloat(expr.Value) * (-1));
            }
            if (expr.Type == Types.Int)
            {
                instructions.Add("uminus");
                return (Types.Int, 0);//return (Types.Int, (int)expr.Value * (-1));
            }
            return (Types.Error, 0);
            
        }
        public override (Types Type, object Value) VisitAssignment([NotNull] MyGrammarParser.AssignmentContext context)
        {
            var right = Visit(context.expression());
            var variable = symbolTable[context.IDENTIFIER().Symbol];
            if (variable.Type == Types.Error || right.Type == Types.Error) return (Types.Error, 0);
            if (variable.Type == Types.Float)
            {
                if (right.Type == Types.Int || right.Type == Types.Float)
                {
                    if (right.Type == Types.Int) instructions.Add("itof");
                    var value = (Types.Float, ToFloat(right.Value));
                    symbolTable[context.IDENTIFIER().Symbol] = value;
                    instructions.Add($"save {context.IDENTIFIER().Symbol.Text}");
                    instructions.Add($"load {context.IDENTIFIER().Symbol.Text}");
                    return value;
                }
            }

            symbolTable[context.IDENTIFIER().Symbol] = right;
            instructions.Add($"save {context.IDENTIFIER().Symbol.Text}");
            instructions.Add($"load {context.IDENTIFIER().Symbol.Text}");
            return right;
        }
        public override (Types Type, object Value) VisitAssignexpr([NotNull] MyGrammarParser.AssignexprContext context)
        {
            return VisitAssignment(context.assignment());
        }
        public override (Types Type, object Value) VisitDeclaration_stat([NotNull] MyGrammarParser.Declaration_statContext context)
        {
            return VisitDeclaration(context.declaration());
        }
        public override (Types Type, object Value) VisitExpression_stat([NotNull] MyGrammarParser.Expression_statContext context)
        {
            Visit(context.expression());
            instructions.Add("pop");
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitRead_stat([NotNull] MyGrammarParser.Read_statContext context)
        {
            return VisitRead_statement(context.read_statement());
        }
        public override (Types Type, object Value) VisitRead_statement([NotNull] MyGrammarParser.Read_statementContext context)
        {
            foreach (var id in context.IDENTIFIER())
            {
                var found = symbolTable[id.Symbol];
                if(found.Type != Types.Error)
                {
                    if (found.Type == Types.Int)
                    {
                        instructions.Add($"read I");
                        instructions.Add($"save {id.Symbol.Text}");
                    }
                    else if (found.Type == Types.Float)
                    {
                        instructions.Add($"read F");
                        instructions.Add($"save {id.Symbol.Text}");
                    }
                    else if (found.Type == Types.Bool)
                    {
                        instructions.Add($"read B");
                        instructions.Add($"save {id.Symbol.Text}");
                    }
                    else if (found.Type == Types.String)
                    {
                        instructions.Add($"read S");
                        instructions.Add($"save {id.Symbol.Text}");
                    }
                }
            }
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitWrite_stat([NotNull] MyGrammarParser.Write_statContext context)
        {
            return VisitWrite_statement(context.write_statement());
        }
        public override (Types Type, object Value) VisitWrite_statement([NotNull] MyGrammarParser.Write_statementContext context)
        {
            int num = 0;
            foreach (var expr in context.expression())
            {
                Visit(expr);
                num++;
            }
            instructions.Add($"print {num}");
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitIf_stat([NotNull] MyGrammarParser.If_statContext context)
        {
            return VisitIf_statement(context.if_statement());
        }
        public override (Types Type, object Value) VisitIf_statement([NotNull] MyGrammarParser.If_statementContext context)
        {
            labelCounter++;
            int ifStart = labelCounter;
            labelCounter++;
            int ifEnd = labelCounter;
            var condition = Visit(context.expression());
            instructions.Add($"fjmp {ifStart}");
            Visit(context.statement()[0]);
            instructions.Add($"jmp {ifEnd}");
            instructions.Add($"label {ifStart}");
            if (context.ELSE() != null)
            {
                Visit(context.statement()[1]);
            }
            instructions.Add($"label {ifEnd}");
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitBlock_stat(MyGrammarParser.Block_statContext context)
        {
            return VisitBlock(context.block());
        }
        public override (Types Type, object Value) VisitBlock(MyGrammarParser.BlockContext context)
        {
            foreach (var statement in context.statement())
            {
                Visit(statement);
            }
            return (Types.Error, 0);
        }
        public override (Types Type, object Value) VisitWhile_stat([NotNull] MyGrammarParser.While_statContext context)
        {
            return VisitWhile_statement(context.while_statement());
        }
        public override (Types Type, object Value) VisitWhile_statement(MyGrammarParser.While_statementContext context)
        {
            labelCounter++;
            int loopStart = labelCounter;
            labelCounter++;
            int loopEnd = labelCounter;
            instructions.Add($"label {loopStart}");
            var val = Visit(context.expression()); // condition
            instructions.Add($"fjmp {loopEnd}");
            Visit(context.statement());
            instructions.Add($"jmp {loopStart}");
            instructions.Add($"label {loopEnd}");
            //labelCounter += 2;
            return (Types.Error, 0);  
        }
        public override (Types Type, object Value) VisitDo_while_stat([NotNull] MyGrammarParser.Do_while_statContext context)
        {
            return VisitDo_while_statement(context.do_while_statement());
        }
        public override (Types Type, object Value) VisitDo_while_statement([NotNull] MyGrammarParser.Do_while_statementContext context)
        {
            labelCounter++;
            int loopStart = labelCounter;
            //labelCounter++;
            //int loopEnd = labelCounter;
            instructions.Add($"label {loopStart}");
            Visit(context.statement());
            var val = Visit(context.expression()); // condition
            instructions.Add($"not");
            instructions.Add($"fjmp {loopStart}");
            //instructions.Add($"label {loopEnd}");
            //Visit(context.statement()); //idkkk T_T
            //instructions.Add($"jmp {loopStart}");
            labelCounter += 2;
            return (Types.Error, 0);  

        }
        
        public void PrintInstructions()
        {
            foreach (var instruction in instructions)
            {
                Console.WriteLine(instruction);
            }
        }
        
        public void InstructionsToFile(string fileName)
        {
            using (StreamWriter file = new StreamWriter(fileName))
            {
                foreach (var instruction in instructions)
                {
                    file.WriteLine(instruction);
                }
            }
        }
    }
}