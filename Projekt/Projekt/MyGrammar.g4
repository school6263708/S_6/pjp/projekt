grammar MyGrammar;

// Keywords

INT:                'int';
FLOAT:              'float';
BOOL:               'bool';
STRING:             'string';
IF:                 'if';
ELSE:               'else';
READ:               'read';
WRITE:              'write';
WHILE:              'while';
DO:                 'do';

// Operators and Delimiters
ASSIGN:             '=';
SEMI:               ';';
COMMA:              ',';
LPAREN:             '(';
RPAREN:             ')';
LBRACE:             '{';
RBRACE:             '}';
LOGIC_OR:           '||';
LOGIC_AND:          '&&';
EQ:                 '==';
NEQ:                '!=';
LT:                 '<';
GT:                 '>';
PLUS:               '+';
MINUS:              '-';
DOT:                '.';
MULT:               '*';
DIV:                '/';
MOD:                '%';
NOT:                '!';

// Literals
DECIMAL_LITERAL:    [0-9]+;
FLOAT_LITERAL:      [0-9]+'.'[0-9]*;
BOOL_LITERAL:       'true'
            |       'false'
            ;
//STRING_LITERAL:     ('"' [a-zA-Z0-9(){}<>,._!?:/*+%=; ]* '-'? [a-zA-Z0-9(){}<>,._!?:/*+%=; ]* '"') | '""';

STRING_LITERAL :    '"' ~["]* '"';

// Identifier
IDENTIFIER:         [a-zA-Z_][a-zA-Z0-9_]*;


// Comments, Whitespace
WS:                 [ \t\r\n\u000C]+ -> channel(HIDDEN);
LINE_COMMENT:       '//' ~[\r\n]*    -> channel(HIDDEN);

//Rules    
program : statement+ EOF ;

statement : 
        declaration (SEMI)+                       #declaration_stat
    |   expression (SEMI)+                        #expression_stat
    |   read_statement (SEMI)+                    #read_stat
    |   write_statement (SEMI)+                   #write_stat
    |   block                                     #block_stat
    |   if_statement                              #if_stat
    |   while_statement                           #while_stat
    |   do_while_statement                        #do_while_stat
    |   SEMI                                      #empty_stat
    ;

declaration : type IDENTIFIER (COMMA IDENTIFIER)*;

type : INT | FLOAT | BOOL | STRING ;

read_statement : READ IDENTIFIER (COMMA IDENTIFIER)* ;

write_statement : WRITE expression (COMMA expression)* ;

block : LBRACE statement+ RBRACE ;

if_statement : IF LPAREN expression RPAREN statement (ELSE statement)? ;

while_statement : WHILE LPAREN expression RPAREN statement ;

do_while_statement : DO statement WHILE LPAREN expression RPAREN SEMI ;

expression : 
        MINUS expression                                #uminusexpr
    |   NOT expression                                  #notexpr
    |   expression op=(MULT|DIV|MOD) expression         #muldivexpr
    |   expression op=(PLUS|MINUS|DOT) expression       #addminexpr
    |   expression op=(LT|GT) expression                #ltgtexpr
    |   expression op=(EQ|NEQ) expression               #eqneqexpr
    |   expression LOGIC_AND expression                 #andorexpr
    |   expression LOGIC_OR expression                  #andorexpr
    |   literal                                         #literalexpr
    |   IDENTIFIER                                      #idexpr
    |   LPAREN expression RPAREN                        #parexpr
    |   assignment                                      #assignexpr
    ;

//op : ASSIGN | LOGIC_OR | LOGIC_AND | EQ | NEQ | LT | GT | PLUS | MINUS | MULT | DIV | MOD | NOT | DOT ;

assignment : <assoc=right> IDENTIFIER ASSIGN expression ;

literal : DECIMAL_LITERAL | FLOAT_LITERAL | STRING_LITERAL | BOOL_LITERAL ;
