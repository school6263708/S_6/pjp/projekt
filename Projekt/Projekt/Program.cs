﻿using System.Globalization;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;

namespace MyApp;

public class Program
{

    public static void Main(string[] args)
    {
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
        var fileName = "/Users/eliska/Documents/School/S_6/PJP/Projekt/Projekt/PLC_t1.in";
        //var fileName = "/Users/eliska/Documents/School/S_6/PJP/Projekt/Projekt/PLC_t2.in";
        //var fileName = "/Users/eliska/Documents/School/S_6/PJP/Projekt/Projekt/PLC_t3.in";
        //var fileName = "/Users/eliska/Documents/School/S_6/PJP/Projekt/Projekt/zozo.in";
        //var fileName = "/Users/eliska/Documents/School/S_6/PJP/Projekt/Projekt/input.in";
        //var fileName = "/Users/eliska/Documents/School/S_6/PJP/Projekt/Projekt/PLC_errors.in";
        Console.WriteLine("Parsing: " + fileName);
        var inputFile = new StreamReader(fileName);
        AntlrInputStream input = new AntlrInputStream(inputFile);
        MyGrammarLexer lexer = new MyGrammarLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MyGrammarParser parser = new MyGrammarParser(tokens);
        
        parser.AddErrorListener(new VerboseListener());
        
        IParseTree tree = parser.program();
        
        if (parser.NumberOfSyntaxErrors == 0)
        {
            //Console.WriteLine(tree.ToStringTree(parser));
            ParseTreeWalker walker = new ParseTreeWalker();
            walker.Walk(new MyGrammarBaseListener(), tree);
        
            new EvalVisitor().Visit(tree);
            if (Errors.NumberOfErrors > 0)
            {
                Errors.PrintAndClearErrors();
                return;
            }
            
            var visitor = new EvalVisitor2();
            visitor.Visit(tree);
            //visitor.PrintInstructions();
            visitor.InstructionsToFile("/Users/eliska/Documents/School/S_6/PJP/Projekt/Projekt/MyOutput.out");
            
            VirtualMachine vm = new VirtualMachine();
            vm.LoadInstructions("/Users/eliska/Documents/School/S_6/PJP/Projekt/Projekt/MyOutput.out");
            Console.WriteLine("Running code...");
            vm.Run();
            Console.WriteLine("Code executed successfully.");
            

        }
        
    }
}