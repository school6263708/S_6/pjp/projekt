using Antlr4.Runtime;

namespace MyApp
{
    public class SymbolTable
    {
        Dictionary<string, (Types Type, object Value)> memory = new();

        public void Add(IToken variable, Types type)
        {
            var name = variable.Text.Trim();
            if (memory.ContainsKey(name))
            {
                Errors.ReportError(variable, $"Variable '{name}' was already declared.");
            }
            else
            {
                if (type == Types.Int) memory.Add(name, (type, 0));
                if (type == Types.Bool) memory.Add(name, (type, false));
                if (type == Types.Float) memory.Add(name, (type, (float)0));
                if (type == Types.String) memory.Add(name, (type, ""));
                //else memory.Add(name, (type, (float)0));
            }
        }
        public (Types Type, object Value) this[IToken variable]
        {
            get {
                var name = variable.Text.Trim();
                if (memory.ContainsKey(name))
                {
                    return memory[name];
                }else
                {
                    Errors.ReportError(variable, $"Variable '{name}' was NOT declared.");
                    return (Types.Error,0);
                }
            }
            set
            {
                var name = variable.Text.Trim();
                memory[name] = value;
            }
        }
    }
}