namespace MyApp;

public enum Types
{
    Int, Float, String, Bool, Error
}