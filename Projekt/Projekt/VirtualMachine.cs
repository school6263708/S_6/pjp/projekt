using System.ComponentModel;
using System.Text;
using System.Xml.Linq;
using Microsoft.VisualBasic;

namespace MyApp;

public class VirtualMachine
{
    private List<string> instructions = new();
    
    private Stack<(Types Type, object Value)> stack = new();
    
    private Dictionary<string, (Types Type, object Value)> variables = new();
    private Dictionary<int, int> labels = new();
    
    private int pointer = 0;
    
    public void LoadInstructions(string path)
    {
        using (var file = new StreamReader(path))
        {
            string line;
            while ((line = file.ReadLine()) != null)
            {
                instructions.Add(line);
                var instructionPart = line.Split(' ');
                if (instructionPart[0] == "label")
                {
                    labels[int.Parse(instructionPart[1])] = instructions.Count - 1;
                }
            }
            
        }
    }

    public void Run()
    {
        while (pointer != instructions.Count)
        { 
            var instructionPart = instructions[pointer].Split(' ');
            string instruction = instructionPart[0];
            bool pointerChanged = false;

            switch (instruction)
            {
                case "push":
                    string instructionType = instructionPart[1];
                    string instructionValue = instructionPart[2];
                    switch (instructionType)
                    {
                        case "I":
                            stack.Push((Types.Int, int.Parse(instructionValue)));
                            break;
                        case "F":
                            stack.Push((Types.Float, float.Parse(instructionValue)));
                            break;
                        case "S":
                            string[] str = instructionPart.Skip(2).ToArray();
                            string temp = string.Join(" ", str);
                            temp = temp.Substring(1, temp.Length - 2);
                            stack.Push((Types.String, temp));
                            break;
                        case "B":
                            stack.Push((Types.Bool, bool.Parse(instructionValue)));
                            break;
                    }
                    break;
                case "pop":
                    stack.Pop();
                    break;
                case "load":
                    string varStr = instructionPart[1];
                    var variable = variables[varStr];
                    stack.Push((variable.Type, variable.Value));
                    break;
                case "save":
                    string varStr2 = instructionPart[1];
                    var value = stack.Pop();
                    variables[varStr2] = value;
                    break;
                case "itof":
                    int val = (int) stack.Pop().Value;
                    stack.Push((Types.Float, (float) val));
                    break;
                case "label":
                    break;
                case "jmp":
                    pointer = labels[int.Parse(instructionPart[1])];
                    pointerChanged = true;
                    break;
                case "fjmp":
                    if (!(bool) stack.Pop().Value)
                    {
                        pointer = labels[int.Parse(instructionPart[1])];
                        pointerChanged = true;
                    }
                    break;
                case "print":
                    List<string> printList = new();
                    for (int i = 0; i < int.Parse(instructionPart[1]); i++)
                    {
                        var val1 = stack.Pop();
                        if (val1.Type == Types.Float && (float)val1.Value % 1 == 0)
                        {
                            float f = (float) val1.Value;
                            printList.Add(f.ToString("#.0"));
                        }
                        else if (val1.Type == Types.Bool)
                        {
                            if ((bool) val1.Value)
                                printList.Add("true");
                            else
                                printList.Add("false");
                        }
                        else
                        {
                            printList.Add(val1.Value.ToString());
                        }
                    }
                    printList.Reverse();
                    Console.WriteLine(Strings.Join(printList.ToArray(), ""));
                    break;
                case "read":
                    switch (instructionPart[1])
                    {
                        case "I":
                            int val2 = int.Parse(Console.ReadLine());
                            stack.Push((Types.Int, val2));
                            break;
                        case "F":
                            float val3 = float.Parse(Console.ReadLine());
                            stack.Push((Types.Float, val3));
                            break;
                        case "S":
                            string s = Console.ReadLine();
                            stack.Push((Types.String, s));
                            break;
                        case "B":
                            bool b = bool.Parse(Console.ReadLine());
                            stack.Push((Types.Bool, b));
                            break;
                    }
                    break;
                case "uminus":
                    var a = stack.Pop();
                    if (a.Type == Types.Float)
                        stack.Push((a.Type, (float) a.Value * (-1)));
                    else
                        stack.Push((a.Type, (int) a.Value * (-1)));
                    break;
                case "not":
                    var a1 = stack.Pop();
                    stack.Push((a1.Type, !(bool) a1.Value));
                    break;
                default:
                    Eval(instruction);
                    break;
            }
            if (!pointerChanged)
            {
                pointer++;
            }
        }
        
    }

    public void Eval(string op)
    {
        var b = stack.Pop();
        var a = stack.Pop();
        
        switch (op)
        {
            case "add":
                if (a.Type == Types.Float)
                    stack.Push((a.Type, (float)a.Value + (float)b.Value));
                else
                    stack.Push((a.Type, (int)a.Value + (int)b.Value));
                break;
            case "sub":
                if (a.Type == Types.Float)
                    stack.Push((a.Type, (float)a.Value - (float)b.Value));
                else
                    stack.Push((a.Type, (int)a.Value - (int)b.Value));
                break;
            case "mul":
                if (a.Type == Types.Float)
                    stack.Push((a.Type, (float)a.Value * (float)b.Value));
                else
                    stack.Push((a.Type, (int)a.Value * (int)b.Value));
                break;
            case "div":
                if (a.Type == Types.Float)
                    stack.Push((a.Type, (float)a.Value / (float)b.Value));
                else
                    stack.Push((a.Type, (int)a.Value / (int)b.Value));
                break;
            case "mod":
                stack.Push((a.Type, (int)a.Value % (int)b.Value));
                break;
            case "concat":
                stack.Push((a.Type, (string) a.Value + (string) b.Value));
                break;  
            case "and":
                stack.Push((a.Type, (bool) a.Value && (bool) b.Value));
                break;
            case "or":
                stack.Push((a.Type, (bool) a.Value || (bool) b.Value));
                break;
            case "lt":
                if (a.Type == Types.Int)
                    stack.Push((a.Type, (int) a.Value < (int) b.Value));
                else
                    stack.Push((a.Type, (float) a.Value < (float) b.Value));
                break;
            case "gt":
                if (a.Type == Types.Int)
                    stack.Push((a.Type, (int) a.Value > (int) b.Value));
                else
                    stack.Push((a.Type, (float) a.Value > (float) b.Value));
                break;
            case "eq":
                if (a.Type == Types.Int)
                    stack.Push((a.Type, (int) a.Value == (int) b.Value));
                else if (a.Type == Types.Float)
                    stack.Push((a.Type, (float) a.Value == (float) b.Value));
                else if (a.Type == Types.String)
                    stack.Push((a.Type, (string) a.Value == (string) b.Value));
                break;

        }
    }
    
    
}